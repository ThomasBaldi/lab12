﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iterators
{
    class Program
    {
        /*
         * == README ==
         * 
         * Il progetto Iterators è finalizzato alla comprensione delle analogie e delle differenze
         * tra gli Stream di Java 8 e gli enumerabili di .Net.
         * 
         * Viene fornita la classe Java8StreamOperations contenente le firme di alcuni metodi
         * estensione aventi gli stessi nomi dei metodi dell'interfaccia Stream di Java 8 
         * (es. map, filter, reduce, etc).
         * 
         * Scopo dell'esercizio è implementare i suddetti metodi affinchè i metodi della classe
         * Java8StreamOperations si comportino come gli omonimi definiti in Java.
         * 
         * Per varificare la correttezza delle proprie implementazioni, l'esercizio prevede infine che
         * la pipeline di elaborazione presente nella classe Program venga riscritta sfruttando
         * i metodi estensione precedentemente implementati.
         */

        static void Main(string[] args)
        {
            const int len = 50;
            int?[] numbers = new int?[len];
            Random rand = new Random();
            for (int i = 0; i < len; i++)
            {
                if (rand.NextDouble() > 0.2)
                {
                    numbers[i] = rand.Next(len);
                }
            }

            // TODO rewrite using methods from Java8StreamOperations
            IDictionary<int, int> occurrences = numbers
                .Select(optN => {
                    Console.Write(optN.ToString() + ",");
                    return optN;
                })
                .Skip(1)
                .Take(len - 2)
                .Where(optN => optN.HasValue)
                .Select(optN => optN.Value)
                .Aggregate(new Dictionary<int, int>(), (d, n) => {
                    if (!d.ContainsKey(n))
                    {
                        d[n] = 1;
                    }
                    else
                    {
                        d[n]++;
                    }
                    return d;
                });

            Console.WriteLine();

            foreach (KeyValuePair<int, int> kv in occurrences)
            {
                Console.WriteLine(kv);
            }

            Console.ReadLine();
        }
    }
}
